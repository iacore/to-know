const std = @import("std");
const FieldType = std.meta.FieldType;
const testing = std.testing;
test {
    testing.refAllDecls(@This());
}

pub const ConceptId = u31;

pub const Concept = union(enum) {
    literal: []const u8,
    compound: [4]?ConceptId,
    rewriting: ConceptId,
};

pub const Memory = struct {
    a: std.mem.Allocator,
    lingered: std.ArrayList(?Concept),
    pub fn init(a: std.mem.Allocator) @This() {
        return .{
            .a = a,
            .lingered = FieldType(@This(), .lingered).init(a),
        };
    }
    pub fn deinit(this: @This()) void {
        this.lingered.deinit();
    }
    pub fn remember(this: *@This(), literal: []const u8) !ConceptId {
        try this.lingered.append(Concept{ .literal = literal });
        return @intCast(this.lingered.items.len - 1);
    }
    pub fn remember__(this: *@This(), a: ?ConceptId, b: ?ConceptId) !ConceptId {
        try this.lingered.append(Concept{ .compound = .{ a, b, null } });
        return @intCast(this.lingered.items.len - 1);
    }
    pub fn remember___(this: *@This(), a: ?ConceptId, b: ?ConceptId, c: ?ConceptId) !ConceptId {
        try this.lingered.append(Concept{ .compound = .{ a, b, c, null } });
        return @intCast(this.lingered.items.len - 1);
    }
    pub fn remember____(this: *@This(), a: ?ConceptId, b: ?ConceptId, c: ?ConceptId, d: ?ConceptId) !ConceptId {
        try this.lingered.append(Concept{ .compound = .{ a, b, c, d } });
        return @intCast(this.lingered.items.len - 1);
    }
    pub fn serialize(this: *@This(), writer: anytype) !void {
        for (0.., this.lingered.items) |i, maybe_concept| {
            const concept = maybe_concept orelse continue;
            try writer.print("{}", .{i});
            switch (concept) {
                .literal => |s| {
                    try writer.writeByte(' ');
                    try writer.writeByte('"');
                    try writer.writeAll(s);
                },
                .compound => |tuple| {
                    try writer.writeByte(' ');
                    for (tuple) |maybe_element_id| {
                        try writer.writeByte('-');
                        if (maybe_element_id) |element_id| {
                            try writer.print("{}", .{element_id});
                        } else {
                            try writer.writeByte('/');
                        }
                    }
                },
                .rewriting => unreachable,
            }
            try writer.writeByte('\n');
        }
    }
    const _awa_ = struct { i: ConceptId, awawa: *?Concept };
    fn lewwwwen(_: void, _lhs: _awa_, _rhs: _awa_) bool {
        const lhs = _lhs.awawa.* orelse return true;
        const rhs = _rhs.awawa.* orelse return false;
        return std.mem.lessThan(u8, &std.mem.toBytes(lhs), &std.mem.toBytes(rhs));
    }
    pub fn eep(this: *@This()) !void {
        const awa = this.lingered.items;
        const _awaawa = try this.a.alloc(_awa_, awa.len);
        defer this.a.free(_awaawa);
        for (0.., awa) |i, *awawa| {
            _awaawa[i] = .{ .i = @intCast(i), .awawa = awawa };
        }
        var awaawa = _awaawa;
        while (true) {
            std.mem.sort(_awa_, awaawa, {}, lewwwwen);
            const start = awawa: {
                for (0..awaawa.len) |i| {
                    if (awaawa[i].awawa.* != null) break :awawa i;
                }
                return;
            };
            awaawa = awaawa[start..];
            var found_duplicate__need_rewrite = false;
            for (0.., 1..awaawa.len) |i_0, i_1| {
                if (std.meta.eql(awaawa[i_0].awawa.*, awaawa[i_1].awawa.*)) {
                    awaawa[i_0].awawa.* = .{ .rewriting = awaawa[i_1].i };
                    found_duplicate__need_rewrite = true;
                }
            }
            if (!found_duplicate__need_rewrite) break;
            for (awaawa) |x| { // retarget: rewrite compound ids
                var concept = x.awawa.* orelse continue;
                defer x.awawa.* = concept;
                switch (concept) {
                    else => continue,
                    .compound => |*elements| {
                        for (elements) |*maybe_element| {
                            var element = maybe_element.* orelse continue;
                            defer maybe_element.* = element;
                            while (_: {
                                switch (awa[element] orelse unreachable // should already be cleaned / no invalid links
                                ) {
                                    .rewriting => |replacing| {
                                        element = replacing;
                                        break :_ true;
                                    },
                                    else => break :_ false,
                                }
                            }) {}
                        }
                    },
                }
            }
        }
        for (awa) |*mawbeawaa| {
            const awaa = mawbeawaa.* orelse continue;
            switch (awaa) {
                .rewriting => {
                    mawbeawaa.* = null;
                },
                else => {},
            }
        }
    }
    test "remember" {
        var mem = Memory.init(testing.allocator);
        defer mem.deinit();
        const concept_ia = try mem.remember("_ia");
        const concept_likes = try mem.remember("likes");
        const concept_object = try mem.remember("   ");
        _ = try mem.remember___(concept_ia, concept_likes, concept_object);
        var buf = std.ArrayList(u8).init(testing.allocator);
        defer buf.deinit();
        try mem.eep();
        try mem.serialize(buf.writer());
        try testing.expectEqualStrings(
            \\0 "_ia
            \\1 "likes
            \\2 "   
            \\3 -0-1-2-/
            \\
        , buf.items);
    }
};
